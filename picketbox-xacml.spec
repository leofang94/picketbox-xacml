%global namedreltag .Final
%global namedversion %{version}%{?namedreltag}
Name:                picketbox-xacml
Version:             2.0.8
Release:             1
Summary:             JBoss XACML
License:             BSD and LGPLv2+
URL:                 http://picketbox.jboss.org/
# svn export http://anonsvn.jboss.org/repos/jbossas/projects/security/security-xacml/tags/2.0.8.Final/ picketbox-xacml-2.0.8.Final
# tar cafJ picketbox-xacml-2.0.8.Final.tar.xz picketbox-xacml-2.0.8.Final
Source0:             picketbox-xacml-%{namedversion}.tar.xz
BuildArch:           noarch
BuildRequires:       maven-local mvn(junit:junit) mvn(org.apache.maven.plugins:maven-release-plugin)
BuildRequires:       mvn(org.jboss:jboss-parent:pom:) mvn(org.picketbox:picketbox-commons)
%description
JBoss XACML Library

%package javadoc
Summary:             Javadoc for %{name}
%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n %{name}-%{namedversion}
%pom_disable_module assembly
%pom_change_dep -r :xml-apis xml-apis: jboss-sunxacml jboss-xacml
cp -p jboss-sunxacml/src/main/resources/licenses/JBossORG-EULA.txt .
cp -p jboss-sunxacml/src/main/resources/licenses/sunxacml-license.txt .
rm .classpath
%mvn_file :jboss-xacml %{name}
%mvn_file :jboss-sunxacml picketbox-sunxacml
%mvn_alias :jboss-xacml org.jboss.security:jbossxacml
%mvn_package ::pom: __noinstall

%build
%mvn_build -f

%install
%mvn_install

%files -f .mfiles
%license JBossORG-EULA.txt

%files javadoc -f .mfiles-javadoc
%license JBossORG-EULA.txt sunxacml-license.txt

%changelog
* Sat Aug 15 2020 zhanghua <zhanghua40@huawei.com> - 2.0.8-1
- Package init
